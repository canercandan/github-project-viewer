# github-project-viewer

Yet another React + GraphQL github client to inspect github organization projects.

## Story

This project was intended to create a yet another github client with a nice looking UX that reads some data from a github repository owner such as listing of github organization repositories or user's repositories as well as getting the data related to this repository and its list of contributors.

**Key features:**

* Search for organizations and users
* List of the repositories related to the selected organization/user and ordered by [starring](https://developer.github.com/v3/activity/starring/).
* Clicking on one repository displays its detailed information as well as a list of collaborators (the collaborators are only visible for repository owned by the token owner (see [here](https://developer.github.com/v3/repos/collaborators/))).

This app was built using React 16+, Apollo-Client 2+ and Material-UI 1+ mostly. It is deployed using a Gitlab-CI setup (see the [.gitlab-ci.yml](./.gitlab-ci.yml) file to have more details about how the project is actually built).

The app is deployed to a AWS S3 bucket following the git branching flow. Whenever a commit is pushed under a branch other than `master` and `production`, the CI auto-deploys the app to the review stage on a newly created bucket therefore a new proper URL will be created so this PR/MR can be reviewed by reviewers. Whenever a branch is merged into `master`, the CI auto-deploys the app to the staging stage therefore the CI will remove an existing staging bucket and create a new one with the same bucket URL so the newly merged version will be able under the staging URL. And same will happens for the `production` branch, a dedicated bucket is there for production verson of the app whenever a new version is merged into `production` branch. The CI also comes along with gitlab-semantic-release which allows to auto-increment the package version following the semver.org semantic and the commitizen syntax (see git cz).

The initial code has been generated from one of my open-source projects [react-packages](https://gitlab.com/4geit/react-packages), thanks to the command `yarn web-builder`, it provides a boilerplate pretty much similar to the famous [create-react-app](https://github.com/facebook/create-react-app) react project generator with a slightly different webpack setup, a pre-defined CI setup and the MobX dependencies and code routines pre-defined in the main files.

Though the app uses Redux reducers to share two local states between the components, I use a better integrated state manager at the Apollo-Client level that comes along with `apollo-link-state`, and many other Apollo linkers and features. The GraphQL queries and mutations, consumed by the React components, will be rendered whenever changes are fired.

The reason I am using a GraphQL client is because Github announced not that long ago the release of a new API based on the GraphQL interface, therefore we can benefits of all the flexibility that GraphQL allows such as defining how the response of a request, whenever query or mutation, has to look like. We basically benefit in speed, bandwidth, and more flexibility with regards to data handling on the frontend. You can read more about the switch of Github to GraphQL [here](https://githubengineering.com/the-github-graphql-api/). You can play with the Github GraphQL API with this [explorer](https://developer.github.com/v4/explorer/) tool.

## Demo

A live demo of the app is available to play with @ http://staging.github-project-viewer.ws3.4ge.it

## Installation

1. A recommended way to install ***github-project-viewer*** is through the [git repository](//gitlab.com/canercandan/github-project-viewer) using the following command:

```bash
git clone git@gitlab.com:canercandan/github-project-viewer.git
```

Or use the HTTPS URL with the following command:

```bash
git clone https://gitlab.com/canercandan/github-project-viewer.git
```

2. Run the command `yarn` to install the project dependencies.

3. You will also need to setup a few environment variable to connect the frontend with the backend (Github GraphQL API endpoint).

Therefore you will need to create a `.env.local` file with the following environment variables set:

* `REACT_APP_GITHUB_PERSONAL_ACCESS_TOKEN`: holds your github personal access token

You can create a github personal access token by visiting the [Developer settings](https://github.com/settings/tokens) section of github and following those steps:

* right into the `Personal access tokens` tab, you will need to generate a new token by click on the `Generate new token` button.
* you will be invited to choose a `Token description`, you can name it after its purpose such as `token for github-project-viewer`
* you will also need to check the following scopes required by the app:
  - `public-repo` under `repo`
  - `read:org` under `admin:org`
* and then you can click on the `Generate token` and it will create you a new token,
* you will end up back in the list section with your newly created token, therefore you can set this token value for the env variable `REACT_APP_GITHUB_PERSONAL_ACCESS_TOKEN` within the `.env.local` as explained above.

4. Because some of the github API types inherits from many interfaces, the apollo-client therefore needs more knowledge about the GraphQL schema of the API, you will therefore needs to run this command in order to retreive the schema file:

```bash
yarn get-fragment-types
```

5. You are now ready to launch the tool with the following command:

```bash
yarn start
```

### Eslint

As part of our linter tool we are using, we also use the graphql plugin to make sure the graphql syntax as well as the queries composition are properly spelled. This plugin requires a manual step in order to fetch the graphql schema, you will therefore need to use the following command line:

```bash
apollo-codegen introspect-schema https://api.github.com/graphql --output schema.json --header "Authorization: Bearer __REACT_APP_GITHUB_PERSONAL_ACCESS_TOKEN__"
```

* Replace `__REACT_APP_GITHUB_PERSONAL_ACCESS_TOKEN__` by the token header if the API requires one

See above to learn how to generate a github personal access token and use it in place of `REACT_APP_GITHUB_PERSONAL_ACCESS_TOKEN`.

Make sure that the newly created `schema.json` file is located at the root of the repository.

If you didn't install `apollo-codegen` yet, we recommend you do it with the command:

```bash
sudo yarn global add apollo-codegen
```
