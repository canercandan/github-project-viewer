/* eslint-disable react/prefer-stateless-function */
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Grid from 'material-ui/Grid'

import { ViewerContainer } from '../containers'
import ProjectList from './project-list'
import ProjectDetails from './project-details'
import SearchBar from './search-bar'

// eslint-disable-next-line
const debug = buildDebug('github-project-viewer:home')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  layout: {
    width: '100vw',
    height: '100vh',
    zIndex: 1,
    overflow: 'hidden',
  },
  searchbar: {
    paddingLeft: 20,
    paddingRight: 20,
  },
  sidebar: {
    minWidth: 200,
  },
  details: {
    marginTop: 20,
    marginLeft: 20,
  },
}))
@withWidth()
@ViewerContainer
export default class Home extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.object.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    selectedRepository: PropTypes.object,
    repositoryOwner: PropTypes.string,
  }
  static defaultProps = {
    selectedRepository: null,
    repositoryOwner: null,
  }

  render() {
    debug('render()')
    const {
      classes, selectedRepository, repositoryOwner,
    } = this.props
    if (!repositoryOwner) {
      return (
        <Grid container spacing={0} alignItems="center" justify="center" className={classes.layout}>
          <Grid item>
            <SearchBar />
          </Grid>
        </Grid>
      )
    }
    return (
      <Fragment>
        <div className={classes.searchbar}>
          <SearchBar />
        </div>
        {/* layout */}
        <Grid container spacing={0} className={classes.content}>
          {/* left sidebar */}
          <Grid item className={classes.sidebar}>
            { repositoryOwner && <ProjectList /> }
          </Grid>
          {/* right content */}
          <Grid item xs className={classes.details}>
            { selectedRepository && <ProjectDetails /> }
          </Grid>
        </Grid>
      </Fragment>
    )
  }
}
