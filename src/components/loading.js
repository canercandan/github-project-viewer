import React from 'react'
import { CircularProgress } from 'material-ui/Progress'
import Grid from 'material-ui/Grid'

const Loading = () => (
  <Grid container alignItems="center" justify="center" spacing={0} style={{ height: '100vh' }}>
    <Grid item>
      <CircularProgress />
    </Grid>
  </Grid>
)

export default Loading
