/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Typography from 'material-ui/Typography'
import GridList, { GridListTile, GridListTileBar } from 'material-ui/GridList'
import InfoIcon from 'material-ui-icons/Info'
import IconButton from 'material-ui/IconButton'

import { RepositoryQuery } from '../graphql'
import Loading from './loading'
import { ViewerContainer } from '../containers'

// eslint-disable-next-line
const debug = buildDebug('github-project-viewer:project-details')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  // TBD
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
  gridList: {
    width: 640,
    // height: 450,
  },
}))
@withWidth()
@ViewerContainer
@RepositoryQuery
export default class ProjectDetails extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.object.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    loading: PropTypes.bool,
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    repository: PropTypes.object,
    // eslint-disable-next-line react/forbid-prop-types
    collaborators: PropTypes.array,
  }
  static defaultProps = {
    loading: true,
    repository: {},
    collaborators: undefined,
  }

  render() {
    debug('render()')
    const {
      classes, loading, repository, collaborators,
    } = this.props
    if (loading) {
      return <Loading />
    }
    const { name, description } = repository
    return (
      // project detail
      <div>
        <Typography variant="headline" gutterBottom>{ name }</Typography>
        <Typography paragraph>{ description }</Typography>
        <Typography variant="subheading" gutterBottom>Collaborators</Typography>
        { collaborators && (
          <GridList cols={4} className={classes.gridList}>
            { collaborators.map(({ login, avatarUrl, url }) => (
              <GridListTile key={login}>
                <img src={avatarUrl} alt={login} />
                <GridListTileBar
                  title={login}
                  actionIcon={
                    <IconButton className={classes.icon} href={url}>
                      <InfoIcon />
                    </IconButton>
                  }
                />
              </GridListTile>
            )) }
          </GridList>
        ) }
        { !collaborators && (
          <Typography variant="caption">Missing access right to view the collaborators of the repository {name}</Typography>
        ) }
      </div>
    )
  }
}
