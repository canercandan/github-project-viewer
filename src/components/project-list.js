/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import List, { ListItem, ListItemText } from 'material-ui/List'

import { RepositoryOwnerQuery } from '../graphql'
import Loading from './loading'
import { ViewerContainer } from '../containers'

// eslint-disable-next-line
const debug = buildDebug('github-project-viewer:project-list')

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  isSelected: {
    backgroundColor: '#eee',
  },
}))
@withWidth()
@ViewerContainer
@RepositoryOwnerQuery
export default class ProjectList extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.object.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    loading: PropTypes.bool,
    // eslint-disable-next-line react/forbid-prop-types
    selectedRepository: PropTypes.object,
    // eslint-disable-next-line react/forbid-prop-types
    repositories: PropTypes.array,
    onSelectedRepositoryChange: PropTypes.func.isRequired,
  }
  static defaultProps = {
    loading: true,
    selectedRepository: null,
    repositories: [],
  }

  handleOnListItemClick = item => () => {
    debug('handleOnListItemClick()')
    debug(item)
    const { onSelectedRepositoryChange } = this.props
    onSelectedRepositoryChange(item)
  }

  render() {
    debug('render()')
    const {
      classes, loading, repositories, selectedRepository,
    } = this.props
    if (loading) {
      return <Loading />
    }
    return (
      // project list
      <List>
        { repositories.map(item => (
          <ListItem
            button
            key={item.id}
            classes={{
              root: selectedRepository && selectedRepository.id === item.id && classes.isSelected,
            }}
            onClick={this.handleOnListItemClick(item)}
          >
            <ListItemText primary={item.name} />
          </ListItem>
        )) }
      </List>
    )
  }
}
