import React, { Component } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { Switch, Route, withRouter } from 'react-router-dom'

import Home from './home'

// eslint-disable-next-line
const debug = buildDebug('github-project-viewer:app')

@withRouter
class App extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    location: PropTypes.object.isRequired,
  }

  render() {
    return (
      <Switch>
        <Route path="/" component={Home} />
      </Switch>
    )
  }
}

export default App
