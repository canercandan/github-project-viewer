/* eslint-disable react/prefer-stateless-function, react/no-multi-comp */
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import buildDebug from 'debug'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import TextField from 'material-ui/TextField'
import Paper from 'material-ui/Paper'
import { MenuItem } from 'material-ui/Menu'
import Downshift from 'downshift'

import { ViewerContainer } from '../containers'
import { SearchQuery } from '../graphql'

// eslint-disable-next-line
const debug = buildDebug('github-project-viewer:search-bar')

@SearchQuery
class SearchResult extends Component {
  static propTypes = {
    inputValue: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    owners: PropTypes.array,
    loading: PropTypes.bool,
    /* eslint-disable react/forbid-prop-types */
    selectedItem: PropTypes.any,
    highlightedIndex: PropTypes.any,
    getItemProps: PropTypes.any,
    /* eslint-enable react/forbid-prop-types */
  }
  static defaultProps = {
    owners: [],
    loading: true,
    selectedItem: undefined,
    highlightedIndex: undefined,
    getItemProps: undefined,
  }

  render() {
    const {
      owners, loading, selectedItem, highlightedIndex, getItemProps,
    } = this.props
    if (loading) {
      return <div />
    }
    return (
      <Fragment>
        { owners.map(({ login }, index) => (
          <MenuItem
            {...getItemProps({ item: login, index })}
            key={login}
            selected={highlightedIndex === index}
            component="div"
            style={{
              fontWeight: selectedItem === login ? 500 : 400,
            }}
          >
            { login }
          </MenuItem>
        )) }
      </Fragment>
    )
  }
}

// eslint-disable-next-line no-unused-vars
@withStyles(theme => ({
  container: {
    flexGrow: 1,
    position: 'relative',
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
}))
@withWidth()
@ViewerContainer
export default class SearchBar extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types, react/no-unused-prop-types
    classes: PropTypes.object.isRequired,
    // eslint-disable-next-line react/no-unused-prop-types
    width: PropTypes.string.isRequired,
    loading: PropTypes.bool,
    // eslint-disable-next-line react/forbid-prop-types
    owners: PropTypes.array,
    repositoryOwner: PropTypes.string,
    onRepositoryOwnerChange: PropTypes.func.isRequired,
  }
  static defaultProps = {
    owners: [],
    loading: true,
    repositoryOwner: null,
  }

  handleOnDownshiftChange = (item) => {
    debug('handleOnDownshiftChange()')
    const { onRepositoryOwnerChange } = this.props
    onRepositoryOwnerChange(item)
  }

  renderInput(inputProps) {
    debug('renderInput()')
    const { repositoryOwner } = this.props
    const {
      InputProps, classes, ref, ...other
    } = inputProps
    InputProps.value = InputProps.value || repositoryOwner
    return (
      <TextField
        {...other}
        inputRef={ref}
        InputProps={{
          classes: {
            input: classes.input,
          },
          ...InputProps,
        }}
      />
    )
  }

  render() {
    debug('render()')
    const { classes } = this.props
    return (
      // search bar
      <Downshift onChange={this.handleOnDownshiftChange}>
        { ({
          getInputProps, getItemProps, isOpen,
          inputValue, selectedItem, highlightedIndex,
        }) => (
          <div className={classes.container}>
            { this.renderInput({
              fullWidth: true,
              classes,
              InputProps: getInputProps({
                placeholder: 'Search a repository owner',
                id: 'integration-downshift',
              }),
            }) }
            { isOpen ? (
              <Paper className={classes.paper} square>
                <SearchResult
                  {...{
                    inputValue,
                    selectedItem,
                    highlightedIndex,
                    getItemProps,
                  }}
                />
              </Paper>
            ) : null }
          </div>
        ) }
      </Downshift>
    )
  }
}
