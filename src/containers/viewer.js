import buildDebug from 'debug'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { setSelectedRepository, setRepositoryOwner } from '../actions'

const debug = buildDebug('github-project-viewer:containers:viewer')

function mapStateToProps(state) {
  debug('mapStateToProps()')
  debug(state)
  const { selectedRepository, repositoryOwner } = state.viewer
  return {
    selectedRepository,
    repositoryOwner,
  }
}
function mapDispatchToProps(dispatch) {
  debug('mapDispatchToProps()')
  debug(dispatch)
  return {
    onSelectedRepositoryChange: bindActionCreators(setSelectedRepository, dispatch),
    onRepositoryOwnerChange: bindActionCreators(setRepositoryOwner, dispatch),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)
