/* eslint-disable no-shadow */
import React from 'react'
import { render } from 'react-dom'
import buildDebug from 'debug'
import { ApolloClient, HttpLink, InMemoryCache, ApolloLink } from 'apollo-boost'
// eslint-disable-next-line
import { IntrospectionFragmentMatcher } from 'apollo-cache-inmemory'
import { withClientState } from 'apollo-link-state'
import { Provider } from 'react-redux'
// to define states
import merge from 'lodash.merge'
import { ApolloProvider } from 'react-apollo'
import { BrowserRouter } from 'react-router-dom'
// load roboto font to entrypoint
// eslint-disable-next-line import/extensions
import 'typeface-roboto'
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles'
import blue from 'material-ui/colors/blue'
import pink from 'material-ui/colors/pink'

import './index.css'
import registerServiceWorker from './registerServiceWorker'

import { App, ErrorBoundary } from './components'
import { configureStore } from './stores'
import introspectionQueryResultData from './fragment-types.json'

// eslint-disable-next-line
const debug = buildDebug('github-project-viewer:index')

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData,
})
const store = configureStore()

// theming the UI
const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: pink,
  },
})

const cache = new InMemoryCache({ fragmentMatcher })
const stateLink = withClientState({
  ...merge(/* XyzState */),
  cache,
})
const httpLink = new HttpLink({
  uri: process.env.REACT_APP_GITHUB_API,
  headers: process.env.REACT_APP_GITHUB_PERSONAL_ACCESS_TOKEN && {
    authorization: `Bearer ${process.env.REACT_APP_GITHUB_PERSONAL_ACCESS_TOKEN}`,
  },
})
const client = new ApolloClient({
  cache,
  link: ApolloLink.from([stateLink, httpLink]),
})

render(
  <ApolloProvider client={client}>
    <BrowserRouter>
      <Provider store={store}>
        <MuiThemeProvider theme={theme}>
          <ErrorBoundary>
            <App />
          </ErrorBoundary>
        </MuiThemeProvider>
      </Provider>
    </BrowserRouter>
  </ApolloProvider>,
  document.getElementById('root'),
)

registerServiceWorker()
