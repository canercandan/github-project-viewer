import { SELECTED_REPOSITORY_SET, REPOSITORY_OWNER_SET } from '../constants'

const initialState = {
  selectedRepository: null,
  repositoryOwner: '',
}

function setSelectedRepository(state, action) {
  const { selectedRepository } = action
  return { ...state, selectedRepository }
}
function setRepositoryOwner(state, action) {
  const { repositoryOwner } = action
  return { ...state, repositoryOwner }
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SELECTED_REPOSITORY_SET:
      return setSelectedRepository(state, action)
    case REPOSITORY_OWNER_SET:
      return setRepositoryOwner(state, action)
    default:
      return state
  }
}
