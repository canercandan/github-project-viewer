import buildDebug from 'debug'
import { graphql } from 'react-apollo'
import { gql } from 'apollo-boost'

import RepositoryFieldsCollaboratorsFragment from '../fragments/repository-fields-collaborators.fragment'

// eslint-disable-next-line
const debug = buildDebug('github-project-viewer:repository-query')

export const query = gql`
  query($owner: String!, $name: String!) {
    repository(owner: $owner, name: $name) {
      ...RepositoryFieldsCollaboratorsFragment
    }
  }
  ${RepositoryFieldsCollaboratorsFragment}
  `
export const config = {
  options: ({ selectedRepository }) => ({
    variables: {
      owner: selectedRepository && selectedRepository.owner.login,
      name: selectedRepository && selectedRepository.name,
    },
    errorPolicy: 'all',
  }),
  props: ({ data: { loading, error, repository } }) => {
    debug('query:repository()')
    debug(repository)
    if (error) { return { loading: false, error, repository } }
    if (loading) { return { loading } }
    let collaborators
    if (repository.collaborators) {
      const { collaborators: { edges: newCollaborators } } = repository
      collaborators = newCollaborators.map(({ node }) => node)
    }
    return { loading: false, repository, collaborators }
  },
}
export default graphql(query, config)
