import buildDebug from 'debug'
import { graphql } from 'react-apollo'
import { gql } from 'apollo-boost'

import RepositoryFieldsFragment from '../fragments/repository-fields.fragment'

// eslint-disable-next-line
const debug = buildDebug('github-project-viewer:repository-owner-query')

export const query = gql`
  query($owner: String!) {
    repositoryOwner(login: $owner) {
      id
      __typename
      login
      repositories(
        first: 10
        orderBy: {
          field: STARGAZERS
          direction: DESC
        }
      ) {
        edges {
          node {
            ...RepositoryFieldsFragment
          }
        }
      }
    }
  }
  ${RepositoryFieldsFragment}
  `
export const config = {
  options: ({ repositoryOwner }) => ({ variables: { owner: repositoryOwner } }),
  props: ({ data: { loading, error, repositoryOwner } }) => {
    debug('query:repositoryOwner()')
    if (loading) { return { loading } }
    if (error) { return { error } }
    let { repositories: { edges: repositories } } = repositoryOwner
    repositories = repositories.map(({ node }) => node)
    return { loading: false, repositoryOwner, repositories }
  },
}
export default graphql(query, config)
