import buildDebug from 'debug'
import { graphql } from 'react-apollo'
import { gql } from 'apollo-boost'

import RepositoryOwnerFieldsFragment from '../fragments/repository-owner-fields.fragment'

// eslint-disable-next-line
const debug = buildDebug('github-project-viewer:search-query')

export const query = gql`
  query($query: String!) {
    search(
      type: USER
      first: 10
      query: $query
    ) {
      edges {
        node {
          ...RepositoryOwnerFieldsFragment
        }
      }
    }
  }
  ${RepositoryOwnerFieldsFragment}
  `
export const config = {
  options: ({ inputValue }) => ({ variables: { query: inputValue } }),
  props: ({ data: { loading, error, search } }) => {
    debug('query:search()')
    if (loading) { return { loading } }
    if (error) { return { error } }
    let { edges: owners } = search
    owners = owners.map(({ node }) => node)
    return {
      loading: false, search, owners,
    }
  },
}
export default graphql(query, config)
