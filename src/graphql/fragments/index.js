export { default as RepositoryOwnerFieldsFragment } from './repository-owner-fields.fragment'
export { default as RepositoryFieldsFragment } from './repository-fields.fragment'
