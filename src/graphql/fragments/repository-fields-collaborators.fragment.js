import { gql } from 'apollo-boost'

import RepositoryFieldsFragment from './repository-fields.fragment'

export default gql`
  fragment RepositoryFieldsCollaboratorsFragment on Repository {
    ...RepositoryFieldsFragment
    collaborators(first: 10) {
      edges {
        node {
          id
          __typename
          login
          avatarUrl
          url
        }
      }
    }
  }
  ${RepositoryFieldsFragment}
  `
