import { gql } from 'apollo-boost'

export default gql`
  fragment RepositoryOwnerFieldsFragment on RepositoryOwner {
    id
    __typename
    login
  }`
