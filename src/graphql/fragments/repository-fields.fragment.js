import { gql } from 'apollo-boost'

export default gql`
  fragment RepositoryFieldsFragment on Repository {
    id
    __typename
    owner {
      id
      __typename
      login
    }
    name
    description
  }`
