import { SELECTED_REPOSITORY_SET, REPOSITORY_OWNER_SET } from '../constants'

export const setSelectedRepository = selectedRepository => ({
  type: SELECTED_REPOSITORY_SET,
  selectedRepository,
})

export const setRepositoryOwner = repositoryOwner => ({
  type: REPOSITORY_OWNER_SET,
  repositoryOwner,
})
