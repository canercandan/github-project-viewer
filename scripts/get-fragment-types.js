/* eslint-disable no-underscore-dangle, no-console */
const fetch = require('node-fetch')
const fs = require('fs')
const util = require('util')
const debug = require('debug')('github-project-viewer:schema')

const writeFile = util.promisify(fs.writeFile)

// load env variables required for fetching the schema
require('dotenv').config({ path: '.env' })
require('dotenv').config({ path: '.env.local' })

debug(process.env.REACT_APP_GITHUB_API)
debug(process.env.REACT_APP_GITHUB_PERSONAL_ACCESS_TOKEN)

async function start() {
  let result
  try {
    result = await fetch(process.env.REACT_APP_GITHUB_API, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${process.env.REACT_APP_GITHUB_PERSONAL_ACCESS_TOKEN}`,
      },
      compress: false,
      body: JSON.stringify({
        query: `
          {
            __schema {
              types {
                kind
                name
                possibleTypes {
                  name
                }
              }
            }
          }
        `,
      }),
    })
  } catch (err) {
    console.error('Error fetching schema', err)
    process.exit(-1)
  }
  try {
    result = await result.json()
    debug(result)
  } catch (err) {
    console.log('Error while converting result to JSON', err)
    process.exit(-1)
  }
  // here we're filtering out any type information unrelated to unions or interfaces
  result.data.__schema.types = result.data.__schema.types
    .filter(type => type.possibleTypes !== null)
  try {
    await writeFile('./src/fragment-types.json', JSON.stringify(result.data))
  } catch (err) {
    console.error('Error writing fragmentTypes file', err)
    process.exit(-1)
  }
  console.log('Fragment types successfully extracted!')
}

start()
